package com.example.fetchrewardsexercise.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.fetchrewardsexercise.api.FetchService
import com.example.fetchrewardsexercise.di.DaggerApiComponent
import com.example.fetchrewardsexercise.model.ItemData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ItemsListViewModel : ViewModel() {

    @Inject
    lateinit var fetchService: FetchService

    init {
        DaggerApiComponent.create().inject(this)
    }

    private val disposable = CompositeDisposable()

    val itemsList = MutableLiveData<List<ItemData>>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val loadErrorText = MutableLiveData<String>()

    // Observing GET API call via RxJava
    fun loadItemsFromApi() {

        loading.value = true
        disposable.add(
            fetchService.getItems()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<ItemData>>() {
                    override fun onSuccess(list: List<ItemData>) {
                        itemsList.value = list
                        loadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        loading.value = false
                        loadErrorText.value = e.message
                    }

                })
        )
    }

    // Clearing disposable that was used for RxJava API call
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}