package com.example.fetchrewardsexercise.api

import com.example.fetchrewardsexercise.model.ItemData
import io.reactivex.Single
import retrofit2.http.GET

interface FetchApi {

    // API Get method for retrofit2 to get Items
    @GET(END_POINT)
    fun getItems(): Single<List<ItemData>>

    companion object {
        //Base API End Point for hiring.json
        private const val END_POINT = "hiring.json"
    }
}