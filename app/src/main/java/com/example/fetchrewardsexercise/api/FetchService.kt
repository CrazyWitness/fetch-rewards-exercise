package com.example.fetchrewardsexercise.api

import com.example.fetchrewardsexercise.di.DaggerApiComponent
import com.example.fetchrewardsexercise.model.ItemData
import io.reactivex.Single
import javax.inject.Inject

class FetchService {

    @Inject
    lateinit var api: FetchApi

    init {
        DaggerApiComponent.create().inject(this)
    }

    // Get Api request that include filter by Page, and sending API Key
    fun getItems(): Single<List<ItemData>> {
        return api.getItems()
    }

}