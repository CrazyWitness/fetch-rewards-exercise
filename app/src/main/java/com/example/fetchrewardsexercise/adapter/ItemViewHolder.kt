package com.example.fetchrewardsexercise.adapter

import android.annotation.SuppressLint
import android.view.View
import com.example.fetchrewardsexercise.model.ItemData
import com.example.fetchrewardsexercise.util.Util.ID
import com.example.fetchrewardsexercise.util.Util.NAME
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import kotlinx.android.synthetic.main.item_data.view.*

// Binding field and data to them for Items elements
class ItemViewHolder(groupView: View): ChildViewHolder(groupView) {

    private val view = groupView

    @SuppressLint("SetTextI18n")
    fun setItem(item: ItemData) {
        view.itemId.text = ID + item.id.toString()
        view.itemName.text = NAME + item.name
    }
}