package com.example.fetchrewardsexercise.adapter

import com.example.fetchrewardsexercise.model.ItemData
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class ListIdExpandableGroup(title: String, items: List<ItemData?>) : ExpandableGroup<ItemData>(title, items)