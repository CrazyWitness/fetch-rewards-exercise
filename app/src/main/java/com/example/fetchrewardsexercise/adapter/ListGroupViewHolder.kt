package com.example.fetchrewardsexercise.adapter

import android.annotation.SuppressLint
import android.view.View
import com.example.fetchrewardsexercise.model.ItemData
import com.example.fetchrewardsexercise.util.Util.LIST_ID
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kotlinx.android.synthetic.main.item_group.view.*

// Binding field and data to them for Groups elements
class ListGroupViewHolder(groupView: View) : GroupViewHolder(groupView) {

    private val view = groupView

    @SuppressLint("SetTextI18n")
    fun setGroupTitle(group: ExpandableGroup<ItemData>) {
        view.groupId.text = LIST_ID + group.title
    }
}