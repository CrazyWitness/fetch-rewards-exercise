package com.example.fetchrewardsexercise.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.fetchrewardsexercise.model.ItemData
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup


class ItemsListAdapter(groups: List<ExpandableGroup<*>?>?) :
    ExpandableRecyclerViewAdapter<ListGroupViewHolder, ItemViewHolder>(groups) {

    // Adapter will use item_group as container for groups
    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int) =
        ListGroupViewHolder(
            LayoutInflater.from(parent?.context)
                .inflate(com.example.fetchrewardsexercise.R.layout.item_group, parent, false)
        )

    // Adapter will use item_data as container for items in the groups
    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int) = ItemViewHolder(
        LayoutInflater.from(parent?.context)
            .inflate(com.example.fetchrewardsexercise.R.layout.item_data, parent, false)
    )

    // Adapter will set title for the groups on position
    override fun onBindGroupViewHolder(
        holder: ListGroupViewHolder?,
        flatPosition: Int,
        group: ExpandableGroup<*>?
    ) {
        holder?.setGroupTitle(group as ExpandableGroup<ItemData>)
    }

    // Adapter will set title for the items in the groups on position
    override fun onBindChildViewHolder(
        holder: ItemViewHolder?,
        flatPosition: Int,
        group: ExpandableGroup<*>?,
        childIndex: Int
    ) {
        val item = (group as ListIdExpandableGroup).items[childIndex]
        holder?.setItem(item)
    }

}