package com.example.fetchrewardsexercise.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fetchrewardsexercise.R
import com.example.fetchrewardsexercise.adapter.ItemsListAdapter
import com.example.fetchrewardsexercise.model.ItemData
import com.example.fetchrewardsexercise.util.Util.setExpandableGroupFromMap
import com.example.fetchrewardsexercise.util.Util.setFilteredAndSortedMap
import com.example.fetchrewardsexercise.viewmodel.ItemsListViewModel
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var itemsListViewModel: ItemsListViewModel
    private lateinit var itemsListAdapter: ItemsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initListViewModel()
        getItemsFromApi()

    }

    // Adding MovieListViewModel to Fragment
    private fun initListViewModel() {
        itemsListViewModel = ViewModelProvider(this).get(ItemsListViewModel::class.java)
    }

    // Adding data to RecyclerView via adapter
    private fun initRecyclerViewAndAdapter(expandableGroup: List<ExpandableGroup<ItemData>>) {
        itemsListAdapter = ItemsListAdapter(expandableGroup)

        listRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = itemsListAdapter
        }

    }


    // Function that loading items from API observing ViewModel Data and showing List with Data or Error based on received Result
    private fun getItemsFromApi() {

        itemsListViewModel.loadItemsFromApi()

        itemsListViewModel.itemsList.observe(this, { itemsList ->
            itemsList?.let {
                listRecyclerView.visibility = View.VISIBLE
                val expandableGroup =
                    setExpandableGroupFromMap(setFilteredAndSortedMap(itemsList))
                initRecyclerViewAndAdapter(expandableGroup)
            }

        })

        itemsListViewModel.loadError.observe(this, { isError ->
            isError?.let {
                listError.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        itemsListViewModel.loading.observe(this, { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    listRecyclerView.visibility = View.GONE
                }
            }
        })

        itemsListViewModel.loadErrorText.observe(this, { errorText ->
            errorText?.let {
                listError.text =
                    if (errorText != "") errorText else getString(R.string.error_text)
            }
        })
    }

}