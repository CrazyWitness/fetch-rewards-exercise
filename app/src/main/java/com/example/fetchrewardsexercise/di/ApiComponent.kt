package com.example.fetchrewardsexercise.di

import com.example.fetchrewardsexercise.api.FetchService
import com.example.fetchrewardsexercise.viewmodel.ItemsListViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(service: FetchService)

    fun inject(viewModel: ItemsListViewModel)

}