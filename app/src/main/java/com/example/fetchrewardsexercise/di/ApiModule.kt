package com.example.fetchrewardsexercise.di

import com.example.fetchrewardsexercise.api.FetchApi
import com.example.fetchrewardsexercise.api.FetchService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule {

    @Provides
    fun provideFetchApi(): FetchApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(FetchApi::class.java)
    }

    @Provides
    fun provideFetchService(): FetchService {
        return FetchService()
    }

    companion object {
        //Base API Url Constant for fetch-hiring
        private const val BASE_URL = "https://fetch-hiring.s3.amazonaws.com/"
    }


}