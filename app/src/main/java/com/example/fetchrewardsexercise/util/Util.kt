package com.example.fetchrewardsexercise.util

import com.example.fetchrewardsexercise.adapter.ListIdExpandableGroup
import com.example.fetchrewardsexercise.model.ItemData
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

object Util {

    // Creating constants to use over the app
    const val LIST_ID = "List Id: "
    const val ID = "Id: "
    const val NAME = "Name: "

    // Building Map and sorting it by:
    // Filter out any items where "name" is blank or null.
    // Sort the results first by "listId" then by "name"
    fun setFilteredAndSortedMap(list: List<ItemData>): Map<Int, List<ItemData>> {
        return list.asSequence()
            .filter { !it.name.isNullOrBlank() }
            .sortedBy { it.name }
            .groupBy { it.listId }
            .toSortedMap(compareBy { it })
    }

    // Creating ExpandableGroup from the Map
    fun setExpandableGroupFromMap(map: Map<Int, List<ItemData>>): List<ExpandableGroup<ItemData>> {
        val expandableGroupsList = mutableListOf<ExpandableGroup<ItemData>>()
        map.forEach { (lisId, itemsData) ->
            expandableGroupsList.add(ListIdExpandableGroup(lisId.toString(), itemsData))
        }
        return expandableGroupsList
    }

}